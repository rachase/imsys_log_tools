
### Notes on Approach 
There are a few ways to analyze a communications systems. One method is to visualize the behavior of the system and look for abnormalities, and then create a long term solution to automate error detection. Another method would be to create software that will perform a stack trace through the logs and generate subplots of individual ids. Although more useful in the long term, this does require initial start up costs and may be harder to visualize without additional effort. While keeping in mind reusability, I will approach this using the first method for two reasons: first is the time constraint, second is that visualization of data can communicate a lot of information at once. Basic classes and methods to assist with searching, sorting, and filtering data will be done in c/c++. Graphs will be generated in matlab as it allows direct access to data while visualizing it.  


**1. Beta to Gamma calls**  
From inspection of the logs, beta is making outgoing calls to gamma which is not an indicated behavior in the instructions.  Initially I believed this to be a typo. However, beta shows downstream calls to epsilon and delta, so I believe this to be some sort of error, or undocumented behavior.  Upon generating new logs centered around the `id`, I saw that this behavior showed up in both beta error logs and beta success logs.  

Example error log showing beta to gamma call
```
id: adb0155c-08cf-45e7-91e9-d5b76cda6df4
A: 2021-03-12T16:27:00.991341, alpha, adb0155c-08cf-45e7-91e9-d5b76cda6df4, 500, call to beta
B: 2021-03-12T16:27:00.998341, beta, adb0155c-08cf-45e7-91e9-d5b76cda6df4, 500, call to gamma
G: 2021-03-12T16:27:01.005341, gamma, adb0155c-08cf-45e7-91e9-d5b76cda6df4, 500, call to delta
D: 2021-03-12T16:27:01.012341, delta, adb0155c-08cf-45e7-91e9-d5b76cda6df4, 500, call to epsilon
E: Not found
```

Example success log showing beta to gamma call
```
id: 9dd56e83-8d07-4b45-a745-d4934a67d429
A: 2021-03-12T03:03:01.352745, alpha, 9dd56e83-8d07-4b45-a745-d4934a67d429, 200, call to beta
B: 2021-03-12T03:03:01.359745, beta, 9dd56e83-8d07-4b45-a745-d4934a67d429, 200, call to gamma
G: 2021-03-12T03:03:01.366745, gamma, 9dd56e83-8d07-4b45-a745-d4934a67d429, 200, call to epsilon
D: Not found
E: 2021-03-12T03:03:01.373745, epsilon, 9dd56e83-8d07-4b45-a745-d4934a67d429, 200, request terminated
```

**2. Visual Overview of Error Events**  
In the image below, the small dots represent terminated calls, and the large dots represent error codes.  Alpha is on the x-axis, Beta on y=1, ... etc.  We can see that terminated calls began early and increase.  However, we can see that errors start all at once, which initially leads me to believe that there was a trigger mechanism (too much CPU usage, comms drop out, etc). 

![quickview](./outputs/images/quickview.png)

Output of the example.cpp executable shows the first error from each log:  

```
First Alpha Error: 2021-03-12T13:13:55.369835
First Beta Error: 2021-03-12T13:13:55.376835
First Gamma Error: 2021-03-12T13:16:55.119272
First Delta Error: 2021-03-12T13:13:55.383835
First Epsilon Error: No errors
```

**4. Detailed Visualization Error Events**  

To best inspect what is happening with the system, we will first compare the Alpha log to the Epsilon and obtain a set of IDs that did not make it through the entire chain, as well as some other data to help us investigate the issue. The file is located in `outputs/analysis/error_end2end_plot.csv`. This will allow me to do some visualization of the data traces more easily. The format is summarized below, but moved to columns for readability:  

```
Id                319e8d3c-7c32-42e9-9bf7-dec5fafa4474  # Transaction ID 
TransactionStart  2021-03-12T00:36:07.654367            # The start time 
Acode             200                          # The code from alpha log
Achild            beta                            # Who alpha is calling
Bcode             200                       # The code from the beta log
Bchild            delta                            # Who beta is calling
Gcode             0                      # No transaction ID in this log
Gchild            0
Dcode             200
Dchild            request terminated           # Request terminated here
Ecode             0
Echild            0
```

The plot below uses that csv to show data traces, starting from alpha, of the id as it propagates through the service.  Microservice levels move upwards, starting with alpha, then beta, etc. Color is tied to the origin of the call: ie, if a call originates from alpha it is red, beta - blue, gamma - green, delta - magenta. If the line is solid, the call was code 200, dotted - code 500.  Terminations are indicated with an x.  For example, if a call originated at alpha, code 200, then went to beta and was terminated, there would be a solid red line that extended upwards to beta connected to a blue x.  

![e2e_trace_overview](./outputs/images/e2e_err_trace.png)

This image tells us much of what we already know.  Services were operating as normal until errors began occurring at around `2021-03-12T13:13:55.369835`. Dropouts do occur in communications, so for investigative purposes, I will assume that the system was acting normally until we see Code 500's: ie, the loss rate up until then is normal behavior (although this is not confirmed).  On that assumption, lets begin to look at the traces with Code 500 only.

![e2e_trace_code500](./outputs/images/e2e_err_trace_code500.png)

The plot shows the error traces only associated with code 500.  Interestingly enough, there are no terminations in this plot, ie, no x's.  In other words, *calls are not being registered at their intended call target.*  Initial thoughts lead me to infer that there is a either a problem with the physical communications layer or the microservices were overloaded in some manner and did not register incoming calls.  However, all calls are stopping at the epsilon service. This can be verified visually on the plot (all traces end at epsilon) as well as by viewing the log file showing end to end errors of code 500 only : [error_end2end_code500.log](outputs/analysis/error_end2end_code500.log).

Since there is no access to the physical layer at the moment, we can investigate load, data rates, and delays.

**5. Microservice Loading**  

I next investigated load percentage over time on each individual node.  This was calculated as number of events over a period of time. The window time in this instance was 20 seconds.  I was not looking for anything in particular, but maybe hoping to catch a heavy load on a node that started the breakdown.  This data is plotted below.  The black vertical line indicates the start of the errors.  Unfortunately, nothing in particular strikes me as causing an error, but running the data through a guassian (or other) filter may reveal some hidden secrets.  

![microservice_load](./outputs/images/microservice_load.png)

Additionally, I also took a look at the time differenct between events, in case there were too many events happening in a node at once.  The images is below, but again, nothing particular jumps out. 

![microservice_load](./outputs/images/event_time_differences_00.png)

A zoomed in version. 

![microservice_load_zoomed](./outputs/images/event_time_differences_01.png)

**5. Call Loading**  

Next up was to examine the call loading. I did this by collecting the calls to each microservice from all logs into one spot.  For example, I searched through each log for "call to epsilon" in order to create a new log of all calls to epsilon. The results of this are below. Again, there was nothing particular that stood out that would cause an issue to the epsilon node.  However, I do see a slight trend upward towards the first dropout.  

![call_load](./outputs/images/call_load.png)

A zoomed in version.

![call_time_diffs](./outputs/images/call_time_differences_00.png)

Call load of epsilon only over a 10 second window.

![call_load_epsilon](./outputs/images/call_load_epsilon.png)

Time difference between calls,epsilon only.

![call_load_epsilon](./outputs/images/call_time_differences_epsilon.png)

**6. Final Results and Next Steps**

A summary of what we have learned:

* Code 500's begin to happen all at once, not gradually over time, as seen from the plot in section 2. 
* The first error occurs at `2021-03-12T13:13:55.369835`, as given from the output of the code.  
* Midroute terminations happen over the whole time period, which we asuume is normal behavior (for now), as seen from the first stack trace plot in section 3. 
* The stack trace plot of all Code 500s show that calls never reached epsilon or epsilon never logged the events, as seen from the second stack trace plot in section 3. 
* We can infer that the failure is in the epsilon node itself, or the physical link from the other nodes to epsilon. The common thread in calls with Code 500 is that they can not reach epsilon. Refer to logfile [error_end2end_code500.log](./outputs/analysis/error_end2end_code500.log)
* From eyeball inspection of plots in section 5, the network loading shows a slight general trend upwards before Code 500's begin, which could suggest a network loading issue.  Further investigation would be needed in order to confirm this.  

Next steps: 

* Overlay the terminations with the load data to look for a relationship
* Overlay the Code 500 events with the load data to look for a relationship
* Look at the time of the Code 500 events for a relationship (time of day, interval of error rate, etc)
* Look at the ID of the Code 500 events on the assumption the epsilon is rejecting the calls  
* Setup a webgui and backend to visualize data easier

**7. Scalability**

This investigation is a mix of visual inspection and presorting of data in order to determine a failure and the cause of it. The software in itself does not determine the result, a human interpreting the data does. In that sense, direct scalability of this method would be difficult as it requires a human in the loop, but elements of this approach can be reused when developing a scalable solution. Visualization in Matlab is great for short term projects in the sense that you can investigate and see the data simultaneously, but isnt made for scalability. I would recommend shifting approaches for long term use. A web interface for interactivity and display using a zeromq or websocket networking layer to connect to the analysis system on the backend would be a good solution.  

However, The [IMSYSLOG class](./src/imsyslog.h) is a start of a class that will assist in search strategies on larger data sets. The approach to developing this class was to load in the log on construction, collect the data into a vector, and be able to rapidly code up ways to make new logs as the investigation progress. In that regards, some scalabity is sacrified for speed to task completion, but the code is intended to be modular and portable so it can be reused. 

The [IMSYSLOG class](./src/imsyslog.h) class offers methods for searching other IMSYSLOG objects via IDs and retrieving relevant information. Now, IMSYSLOG object loads in the log file at construction time, which is great for fast searching, but may cause memory issues when search much larger data sets.  A alternate strategy would be to open the log file and search for elements, storing them locally on an as needed basis and/or implementing a local cache. 

Additionally, the code used to create supplementary logs was made for on the fly usage and quick changes. For larger data sets, a more automated, object-oriented approach would be preferred: ie:  
```
CreateLogFromId(std::vector<std::string>logfiles, std::string id);
```
instead of
```
alpha_log.GetEventString(std::string id);
beta_log.GetEventString(std::string id);
gamma_log.GetEventString(std::string id);
...etc
```
