# imsys_log_tools

Log Parsing and Analysis tools for Imaginary System's epsilon service

### Notes on project

The C/C++ code in this project extracts information into super awesome log and csv files, which are then made into plots via Matlab. Results are discussed in [ANALYSIS.md](ANALYSIS.md). 

### Building and Running

A build script, `build.sh`, is provided in the home directory (its 2 lines). This software requires boostlib, which can be installed via `sudo apt-get install libboost-dev`.  To clone and build:
```
git clone https://gitlab.com/rachase/imsys_log_tools.git
cd imsys_log_tools/
./build.sh 
./bin/runexample 
```

Executing the runexample will generate the logs and csv's used for analysis and plotting. All plots and logs that are generated from the code are already stored in this repo in the `./outputs/` folder. 

Plots are generated using Matlab. If you can not run the scripts, Matlab figures are provided in the `./matlab/matlabfigures/` directory. Additionally, 4k screenshots of these figures are located in `./outputs/images/` folder. Scripts to generate the figures must be run from the directory that they are in, ie `./matlab/`. 

Full discussion of the approach and analysis is in the [ANALYSIS.md](ANALYSIS.md) file.