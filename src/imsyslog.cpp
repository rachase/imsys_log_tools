#include "imsyslog.h"

//initalize IMSYSLOG object
//Loads the file, gets indices for terminations, errors, successes
IMSYSLOG::IMSYSLOG(std::string s)
{
  std::cout << "Loading log file " << s << std::endl;

  LoadLog(s);
  GetErrorSuccessIndicies();
  GetTermIndicies();
}

//convert the text string to an int
int IMSYSLOG::Text2Int(std::string s)
{
  int retval = -1;
  if(s == "request terminated") retval = 0;
  if(s == "call to alpha")retval = 1;
  if(s == "call to beta")retval = 2;
  if(s == "call to gamma")retval = 3;
  if(s == "call to delta")retval = 4;
  if(s == "call to epsilon")retval = 5;

  return retval;
}

//convert the code string to an int
int IMSYSLOG::Code2Int(std::string s)
{
  int retval = -1;
  if(s == "500") retval = 0;
  if(s == "200")retval = 1;
  
  return retval;
}

//output this log to CSV for external use
void IMSYSLOG::Log2CSV(std::string s)
{
  std::ofstream file;
  file.open (s);

  int result=-1;
  for (int i = 0 ; i<events.size() ; i++ )
  {

    file << events.at(i).ts << "," 
         << events.at(i).source << "," 
         << events.at(i).id << "," 
         << events.at(i).code << "," 
         << events.at(i).text << std::endl;
  }

  file.close();
}

//read in the json data and store into vector containers.
//brute force json reader for now, boost library 1.65 has issues
//not ideal but its working. 
void IMSYSLOG::JsonStr2Vec(std::string s)
{
  int index;
  std::vector<std::string> s_vec(5);

  std::string local_ts,
              local_source,
              local_id,
              local_code,
              local_text;

  //convert json to csv, split on ',', operate on strings
  boost::replace_all(s, "{\"ts\":\"", "");
  boost::replace_all(s, "\", \"source\":\"", ",");
  boost::replace_all(s, "\", \"id\":\"", ",");
  boost::replace_all(s, "\", \"code\":", ",");
  boost::replace_all(s, ", \"text\":\"", ",");
  boost::replace_all(s, "\"}", "");

  boost::split(s_vec,s,boost::is_any_of(","));

  call_event e;
  e.ts = s_vec.at(0);
  e.source = s_vec.at(1);
  e.id = s_vec.at(2);
  e.code = s_vec.at(3);
  e.text = s_vec.at(4);

  events.push_back(e);

}

//Open the log file, read it into a vector, close file
void IMSYSLOG::LoadLog(std::string s)
{
  std::string line;
  std::ifstream f(s);

  if (f.is_open())
  {
    while ( getline (f,line) )
    {
      //std::cout << line << '\n';
      JsonStr2Vec(line);
    }
    f.close();
  }
  else
  {
     std::cout << "Unable to open " << s << std::endl; 
  }
}

//find the first error in the log
std::string IMSYSLOG::FindFirstError(void)
{
  std::string retval = "No errors";
  for (int i = 0; i < events.size(); i++ )
  {
    if (events.at(i).code == "500")
    {
      retval = events.at(i).ts;
      i = events.size()+10; 
    }
  }

  return retval;
}

//adds indices of all errors / successes to vectors
void IMSYSLOG::GetErrorSuccessIndicies(void)
{
  for (int i = 0; i < events.size(); i++ )
  {
    if (events.at(i).code == "500")
    {
      error_indices.push_back(i);
    }
    if (events.at(i).code == "200")
    {
      success_indices.push_back(i);
    }
  }
}

//adds indices of all terminations to a vector
void IMSYSLOG::GetTermIndicies(void)
{
  for (int i = 0; i < events.size(); i++ )
  {
    if (events.at(i).text == "request terminated")
    {
      term_indices.push_back(i);
    }
  }
}

//pass in a id string and it returns the event associated with it, csv style
std::string IMSYSLOG::GetEventString(std::string thisid)
{
  std::string retval = "Not found";
  
  for (int i = 0; i < events.size(); i++ )
  {
    if (events.at(i).id == thisid)
    {
      retval =  events.at(i).ts + ", " +
                events.at(i).source + ", " +
                events.at(i).id + ", " +
                events.at(i).code + ", " +
                events.at(i).text;
      i = events.size()+10;
    }
  }

  return retval;
}

//helper for making a csv for plotting
std::string IMSYSLOG::GetEventString4plot(std::string thisid)
{
  std::string retval;
  std::string scratch;
  int i = GetIDIndex(thisid);

  if (i>=0)
  {
    scratch = events.at(i).text;
    boost::replace_all(scratch, "call to ", "");

    retval = events.at(i).code
             + ", " 
             + scratch;
  }
  else
  {
    retval = "0, 0";
  }
  
  return retval;
}

//pass in a id string and it returns the index it is stored at, -1 if not found
int IMSYSLOG::GetIDIndex(std::string thisid)
{
  int retval = -1;
  
  for (int i = 0; i < events.size(); i++ )
  {
    if (events.at(i).id == thisid)
    {
      retval =  i;
      i = events.size()+10;
    }
  }

  return retval;
}

// given a vector, search it for similar ids of this log
// returns matching indices of this log
std::vector<int> IMSYSLOG::FindSimilar(IMSYSLOG * targetlog)
{
  std::vector<int> retval;
  int q;
  for(int i = 0; i<events.size() ;i++ )
  {
    q = targetlog->GetIDIndex(events.at(i).id);
    if (q >= 0)
    {
      retval.push_back(i);
    }
  }
  return retval;
}

// given a vector, search it for missing ids of this log
// returns matching indices of this log
std::vector<int> IMSYSLOG::FindMissing(IMSYSLOG * targetlog)
{
  std::vector<int> retval;
  int q;
  for(int i = 0; i<events.size() ;i++ )
  {
    q = targetlog->GetIDIndex(events.at(i).id);
    if (q == -1)
    {
      retval.push_back(i);
    }
  }
  return retval;
}
