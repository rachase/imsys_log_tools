#include "main.h"


int main()
{
  std::cout << std::endl;

  std::cout << "Finding First Errors (Code 500):" << std::endl;
  find_first_errors();
  std::cout << std::endl;

  std::cout << "Generating additional logs and csvs...";
  create_new_logs();
  std::cout << "Done." << std::endl;

  std::cout << std::endl;
  return 0;
}
