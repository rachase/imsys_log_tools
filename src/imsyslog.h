#pragma once

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>

class IMSYSLOG
{

  public:

  //log entries put into call event structure. 
  struct call_event
  {
    std::string ts;
    std::string source;
    std::string id;
    std::string code;
    std::string text;
  };

  std::vector<call_event> events;

  //indicies that points to events vector
  std::vector<int> success_indices;
  std::vector<int> error_indices;
  std::vector<int> term_indices;
  
  //Construction, requires file path to log
  IMSYSLOG(std::string s);

  //Function for loading log from file into event structure
  void LoadLog(std::string s);
  //Dumps the event structure to a CSV
  void Log2CSV(std::string s);
  
  
  //analysis tools
  //FindSimilar - Find ids in this log that are in another
  std::vector<int> FindSimilar(IMSYSLOG * targetlog);   
  //FindSimilar - Find ids in this log that are not in another
  std::vector<int> FindMissing(IMSYSLOG * targetlog); 
  //Finds the first error in this log  
  std::string FindFirstError(void);
  //Returns the storage index associated with a given ID
  int GetIDIndex(std::string thisid);
  //Get a CSV string associated with a given ID
  std::string GetEventString(std::string thisid);
  //Get a CSV string of only the code and call to target
  std::string GetEventString4plot(std::string thisid);  

  private:

  // some helpers to convert log values to ints
  int Text2Int(std::string s);
  int Code2Int(std::string s);

  // Reads in the JSON log and dumps to event structure
  // this is fragile, but working.
  void JsonStr2Vec(std::string s);
  // gets the indicies of errors and success stores to vector
  void GetErrorSuccessIndicies(void);
  // gets the indicies terminations and stores to vector
  void GetTermIndicies(void);


};