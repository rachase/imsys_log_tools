#include "example.h"


//vars for alpha, beta, delta, epsilon, and gamma logs
IMSYSLOG a_log("./example_logs/alpha.log");  
IMSYSLOG b_log("./example_logs/beta.log");  
IMSYSLOG d_log("./example_logs/delta.log");  
IMSYSLOG e_log("./example_logs/epsilon.log");  
IMSYSLOG g_log("./example_logs/gamma.log");  

//output some csv-ish logs for external use (matlab)
void logs_to_csv()
{
  a_log.Log2CSV("./outputs/log_csvs/alpha.csv");  
  b_log.Log2CSV("./outputs/log_csvs/beta.csv");  
  d_log.Log2CSV("./outputs/log_csvs/delta.csv");  
  e_log.Log2CSV("./outputs/log_csvs/epsilon.csv");  
  g_log.Log2CSV("./outputs/log_csvs/gamma.csv");  
}

//looks for successes (Code 200) in the log, grabs the id
//finds those ids in the other logs, and outputs them to one file
void success_log_from_id(IMSYSLOG * log,std::string s)
{
  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/success_ids_" + s + ".log");
  for (int i=0 ; i< log->success_indices.size() ; i++)
  {
      index = log->success_indices.at(i);
      id_string = log->events.at(index).id;
      file << "id: " << id_string << std::endl;
      file << "A: " << a_log.GetEventString(id_string) << std::endl;
      file << "B: " << b_log.GetEventString(id_string) << std::endl;
      file << "G: " << g_log.GetEventString(id_string) << std::endl;
      file << "D: " << d_log.GetEventString(id_string) << std::endl;
      file << "E: " << e_log.GetEventString(id_string) << std::endl;
      file << std::endl;
  }
  file.close();
}

//looks for errors (Code 500) in the log, grabs the id
//finds those ids in the other logs, and outputs them to one file
void error_log_from_id(IMSYSLOG * log,std::string s)
{
  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/error_ids_" + s + ".log");
  for (int i=0 ; i< log->error_indices.size() ; i++)
  {
      index = log->error_indices.at(i);
      id_string = log->events.at(index).id;
      file << "id: " << id_string << std::endl;
      file << "A: " << a_log.GetEventString(id_string) << std::endl;
      file << "B: " << b_log.GetEventString(id_string) << std::endl;
      file << "G: " << g_log.GetEventString(id_string) << std::endl;
      file << "D: " << d_log.GetEventString(id_string) << std::endl;
      file << "E: " << e_log.GetEventString(id_string) << std::endl;
      file << std::endl;
  }
  file.close();
}

//function to find the first errors in the system.  
void find_first_errors()
{
  //find first error so we have a general idea of where things started.
  //matlab plot visually indicates that errors started around the same time
  std::cout << "First Alpha Error: " << a_log.FindFirstError() << std::endl;
  std::cout << "First Beta Error: " << b_log.FindFirstError() << std::endl;
  std::cout << "First Gamma Error: " << g_log.FindFirstError() << std::endl;
  std::cout << "First Delta Error: " << d_log.FindFirstError() << std::endl;
  std::cout << "First Epsilon Error: " << e_log.FindFirstError() << std::endl;
}

//makes a log of successfull of end to end transmissions, puts them in one file. 
void find_end2end_successes()
{
  std::vector<int> success_indices;
  success_indices = a_log.FindSimilar(&e_log);

  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/success_end2end.log");

  for (int i=0 ; i< success_indices.size() ; i++)
  {
      index = success_indices.at(i);
      id_string = a_log.events.at(index).id;
      file << "id: " << id_string << std::endl;
      file << "A: " << a_log.GetEventString(id_string) << std::endl;
      file << "B: " << b_log.GetEventString(id_string) << std::endl;
      file << "G: " << g_log.GetEventString(id_string) << std::endl;
      file << "D: " << d_log.GetEventString(id_string) << std::endl;
      file << "E: " << e_log.GetEventString(id_string) << std::endl;
      file << std::endl;
  }
  file.close();
}

//makes a log of failures of end to end transmissions, puts them in one file. 
void find_end2end_failures()
{
  std::vector<int> error_indices;
  error_indices = a_log.FindMissing(&e_log);

  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/error_end2end.log");

  for (int i=0 ; i< error_indices.size() ; i++)
  {
      index = error_indices.at(i);
      id_string = a_log.events.at(index).id;
      file << "id: " << id_string << std::endl;
      file << "A: " << a_log.GetEventString(id_string) << std::endl;
      file << "B: " << b_log.GetEventString(id_string) << std::endl;
      file << "G: " << g_log.GetEventString(id_string) << std::endl;
      file << "D: " << d_log.GetEventString(id_string) << std::endl;
      file << "E: " << e_log.GetEventString(id_string) << std::endl;
      file << std::endl;
  }
  file.close();
}

//makes a log of failures of end to end transmissions, code 500 only, puts them in one file. 
void find_end2end_failures_500()
{
  std::vector<int> error_indices;
  error_indices = a_log.FindMissing(&e_log);

  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/error_end2end_code500.log");

  for (int i=0 ; i< error_indices.size() ; i++)
  {
      index = error_indices.at(i);
      if(a_log.events.at(index).code=="500")
      {
        id_string = a_log.events.at(index).id;
        file << "id: " << id_string << std::endl;
        file << "A: " << a_log.GetEventString(id_string) << std::endl;
        file << "B: " << b_log.GetEventString(id_string) << std::endl;
        file << "G: " << g_log.GetEventString(id_string) << std::endl;
        file << "D: " << d_log.GetEventString(id_string) << std::endl;
        file << "E: " << e_log.GetEventString(id_string) << std::endl;
        file << std::endl;
      }
  }
  file.close();
}

//makes a log of failures of end to end transmissions, puts them in one file. 
void make_end2end_failures_csv_plot()
{
  std::vector<int> error_indices;
  error_indices = a_log.FindMissing(&e_log);

  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/error_end2end_plot.csv");

  file << "Id, TransactionStart, Acode, Achild, Bcode, Bchild, Gcode, Gchild, Dcode, Dchild, Ecode, Echild" << std::endl;
  
  for (int i=0 ; i< error_indices.size() ; i++)
  {
      index = error_indices.at(i);
      id_string = a_log.events.at(index).id;
      file  << id_string << ", " 
            << a_log.events.at(index).ts << ", " 
            << a_log.GetEventString4plot(id_string) << ", " 
            << b_log.GetEventString4plot(id_string) << ", " 
            << g_log.GetEventString4plot(id_string) << ", " 
            << d_log.GetEventString4plot(id_string) << ", " 
            << e_log.GetEventString4plot(id_string) 
            << std::endl;
  }
  file.close();
}

//makes a plottable csv of all messages
void make_end2end_csv_plot()
{
  std::vector<int> error_indices;
  error_indices = a_log.FindMissing(&e_log);

  int index;
  std::string id_string;

  std::ofstream file;
  file.open ("./outputs/analysis/end2end_plot.csv");

  file << "Id, TransactionStart, Acode, Achild, Bcode, Bchild, Gcode, Gchild, Dcode, Dchild, Ecode, Echild" << std::endl;
  
  for (int i=0 ; i< a_log.events.size() ; i++)
  {
      id_string = a_log.events.at(i).id;
      file  << id_string << ", " 
            << a_log.events.at(i).ts << ", " 
            << a_log.GetEventString4plot(id_string) << ", " 
            << b_log.GetEventString4plot(id_string) << ", " 
            << g_log.GetEventString4plot(id_string) << ", " 
            << d_log.GetEventString4plot(id_string) << ", " 
            << e_log.GetEventString4plot(id_string) 
            << std::endl;
  }
  file.close();
}

void create_new_logs() {

  //used for matlab. Probably deletable
  logs_to_csv();

  //generate logs organized by id for visual inspection
  error_log_from_id(&a_log,"alpha");
  error_log_from_id(&b_log,"beta");
  error_log_from_id(&g_log,"gamma");
  error_log_from_id(&d_log,"delta");
  error_log_from_id(&e_log,"epsilon");

  success_log_from_id(&a_log,"alpha");
  success_log_from_id(&b_log,"beta");
  success_log_from_id(&g_log,"gamma");
  success_log_from_id(&d_log,"delta");
  success_log_from_id(&e_log,"epsilon");

  find_end2end_successes();
  find_end2end_failures();
  find_end2end_failures_500();

  make_end2end_csv_plot();
  make_end2end_failures_csv_plot();
  
  
}