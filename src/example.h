#pragma once
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "imsyslog.h"

//main routine that analysis files
void create_new_logs(void);

//makes a csv of failures of end to end transmissions, puts them in one file. 
void make_end2end_failures_csv_plot(void);

//makes a csv of end to end transmissions, puts them in one file. 
void make_end2end_csv_plot(void);

//makes a log of failures of end to end transmissions, puts them in one file. 
void find_end2end_failures(void);

//makes a log of successfull of end to end transmissions, puts them in one file. 
void find_end2end_successes(void);

//function to find the first errors in the system.  
void find_first_errors(void);

//looks for errors (Code 500) in the log, grabs the id
//finds those ids in the other logs, and outputs them to one file
void error_log_from_id(IMSYSLOG * log,std::string s);

//looks for successes (Code 200) in the log, grabs the id
//finds those ids in the other logs, and outputs them to one file
void success_log_from_id(IMSYSLOG * log,std::string s);

//output some csv-ish logs for external use (matlab)
void logs_to_csv();

extern IMSYSLOG a_log;  
extern IMSYSLOG b_log;
extern IMSYSLOG d_log;
extern IMSYSLOG e_log;
extern IMSYSLOG g_log;