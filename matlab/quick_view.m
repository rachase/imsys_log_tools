%quick view for initial inspection of data.  

%% Read in the data
clear; 

verbose=true;

path = '../outputs/log_csvs/';

% Read in CSVs
A = readtable(append(path,'alpha.csv'));
B = readtable(append(path,'beta.csv'),'Delimiter',',');
G = readtable(append(path,'gamma.csv'));
D = readtable(append(path,'delta.csv'));
E = readtable(append(path,'epsilon.csv'));

%Get indices of errors
A_err = find(A.Var4 ==500);
B_err = find(B.Var4 ==500);
G_err = find(G.Var4 ==500);
D_err = find(D.Var4 ==500);
E_err = find(E.Var4 ==500);

%Get Terminations
A_term = find(A.Var5 == "request terminated");
B_term = find(B.Var5 == "request terminated");
G_term = find(G.Var5 == "request terminated");
D_term = find(D.Var5 == "request terminated");
E_term = find(E.Var5 == "request terminated");


%Read in Datetimes.
Adt=datetime(A.Var1,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');
Bdt=datetime(B.Var1,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');
Gdt=datetime(G.Var1,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');
Ddt=datetime(D.Var1,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');
Edt=datetime(E.Var1,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');


%Lets look at errors and terminated calls
figure(1)
hold on;
plot(Adt(A_err),A.Var4(A_err)-500,'r.','MarkerSize',20);
plot(Bdt(B_err),B.Var4(B_err)-499,'k.','MarkerSize',20);
plot(Gdt(G_err),G.Var4(G_err)-498,'b.','MarkerSize',20);
plot(Ddt(D_err),D.Var4(D_err)-497,'g.','MarkerSize',20);
plot(Edt(E_err),E.Var4(E_err)-496,'m.','MarkerSize',20);

yval = zeros(length(A_term))+0;
plot(Adt(A_term),yval,'rx','MarkerSize',2);
yval = zeros(length(B_term))+1;
plot(Bdt(B_term),yval,'kx','MarkerSize',2);
yval = zeros(length(G_term))+2;
plot(Gdt(G_term),yval,'bx','MarkerSize',2);
yval = zeros(length(D_term))+3;
plot(Ddt(D_term),yval,'gx','MarkerSize',2);
yval = zeros(length(E_term))+4;
plot(Edt(E_term),yval,'mx','MarkerSize',2);

ylim([-1 20])
hold off


%lets plot the quantity of terminated calls
