%Script to plot data from the end2end error log.  

%Plot description: This plots the relationships of IDs in the system
%that did not make it through from alpha to epsilon. Colors represent 
%the microservice that initiated the call.  A colored line will begin
%at the microservice it initiated from and extend upwards to its target
%microservice.  If the call was Code = 200, the line will be solid.  If
%the call was Code = 500, the line will be dotted.  Colored x's indicate
%where the call was terminated.


%% Read in the data
clear; 

verbose=true;

path = '../outputs/analysis/';

% Read in CSVs
e2e_err = readtable(append(path,'end2end_plot.csv'),'Delimiter',',','Format','%s%s%s%s%s%s%s%s%s%s%s%s');

event_start_times=datetime(e2e_err.TransactionStart,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');
setsize = length(event_start_times);
offset = zeros(setsize,1);

figure(1);
set(gca,'FontSize',18)
hold on;

%Plot A's

success_idx = find(e2e_err.Acode == "200");
fail_idx = find(e2e_err.Acode == "500");
term_idx = find(e2e_err.Achild == "request terminated");
offset = zeros(setsize,1);

for i = 1:setsize
    
    if (e2e_err.Achild(i) == "beta")
        offset(i)=1;
    end
    
    if (e2e_err.Achild(i) == "gamma")
        offset(i)=2;
    end
    
end

plot_microservice(term_idx,fail_idx,success_idx,event_start_times,offset,'Red',0);

%Plot B's

success_idx = find(e2e_err.Bcode == "200");
fail_idx = find(e2e_err.Bcode == "500");
term_idx = find(e2e_err.Bchild == "request terminated");
offset = zeros(setsize,1);

for i = 1:setsize
    
    if (e2e_err.Bchild(i) == "gamma")
        offset(i)=1;
    end
    
    if (e2e_err.Bchild(i) == "delta")
        offset(i)=2;
    end
    
    if (e2e_err.Bchild(i) == "epsilon")
        offset(i)=3;
    end
    
end

plot_microservice(term_idx,fail_idx,success_idx,event_start_times,offset,'Blue',1);

%Plot G's

success_idx = find(e2e_err.Gcode == "200");
fail_idx = find(e2e_err.Gcode == "500");
term_idx = find(e2e_err.Gchild == "request terminated");
offset = zeros(setsize,1);

for i = 1:setsize
    
    if (e2e_err.Gchild(i) == "delta")
        offset(i)=1;
    end
    
    if (e2e_err.Gchild(i) == "epsilon")
        offset(i)=2;
    end
    
end

plot_microservice(term_idx,fail_idx,success_idx,event_start_times,offset,'#228B22',2);

%Plot D's

success_idx = find(e2e_err.Dcode == "200");
fail_idx = find(e2e_err.Dcode == "500");
term_idx = find(e2e_err.Dchild == "request terminated");
offset = zeros(setsize,1);

for i = 1:setsize
    
    if (e2e_err.Dchild(i) == "epsilon")
        offset(i)=1;
    end
    
end

plot_microservice(term_idx,fail_idx,success_idx,event_start_times,offset,'Magenta',3);

yline(0,'-','Alpha Service','LineWidth',2,'LabelHorizontalAlignment' ,'left');
yline(1,'-','Beta Service','LineWidth',2,'LabelHorizontalAlignment' ,'left');
yline(2,'-','Gamma Service','LineWidth',2,'LabelHorizontalAlignment' ,'left');
yline(3,'-','Delta Service','LineWidth',2,'LabelHorizontalAlignment' ,'left');
yline(4,'-','Epsilon Service','LineWidth',2,'LabelHorizontalAlignment' ,'left');
xlabel("Time of Dropped Packet");
ylabel("Microservices");
title("ID trace vs time (Code 500 Only)");
yticks([0 1 2 3 4]);
ylim ([-.1 4.1]);

hold off;


function a = plot_microservice(t,f,s,st,o,color,shift)
% Plots the lines between microservices.
% f=fail_idx,s=success_idx,st=starttimes,o=offset,color,
% shift = the starting number of the microservice A=0, B=1...

    % plot successfull calls
    X1 = st(s);
    Y1 = zeros(length(s),1)+shift;
    Y2 = o(s)+shift;
    plot([X1'; X1'], [Y1'; Y2'],'Color',color,'LineStyle','-');

    % plot unsuccessfull calls
    X1 = st(f);
    Y1 = zeros(length(f),1)+shift;
    Y2 = o(f)+shift;
    plot([X1'; X1'], [Y1'; Y2'],'Color',color,'LineStyle','--');
    
    % plot terminations
    t_fail = intersect(f,t);
    plot(st(t_fail),zeros(length(t_fail),1)+shift,'Color',color,'Marker','x','MarkerSize',20);
    

end

