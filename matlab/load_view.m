%plots loads of microservices.  

%% Read in the data
clear; 

verbose=true;

path = '../outputs/log_csvs/';

first_error_time = datetime('2021-03-12T13:13:55.369835','InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');

% Read in CSVs
A = readtable(append(path,'alpha.csv'));
B = readtable(append(path,'beta.csv'),'Delimiter',',');
G = readtable(append(path,'gamma.csv'));
D = readtable(append(path,'delta.csv'));
E = readtable(append(path,'epsilon.csv'));

%% Make some super cool data to look at
%times of events
Atimes  = iso_to_datetime(A.Var1);
Btimes = iso_to_datetime(B.Var1);
Gtimes = iso_to_datetime(G.Var1);
Dtimes = iso_to_datetime(D.Var1);
Etimes = iso_to_datetime(E.Var1);

%times differences between events
Atimes_diff = seconds(diff(Atimes));
Btimes_diff = seconds(diff(Btimes));
Gtimes_diff = seconds(diff(Gtimes));
Dtimes_diff = seconds(diff(Dtimes));
Etimes_diff = seconds(diff(Etimes));

%load percentages
window = 20;
Aload = calc_load(Atimes,window);
Bload = calc_load(Btimes,window);
Gload = calc_load(Gtimes,window);
Dload = calc_load(Dtimes,window);
Eload = calc_load(Etimes,window);

%% More cool data: Timestamps all the calls to nodes and sort
[Acalls,Bcalls,Gcalls,Dcalls,Ecalls] = find_calls(A,B,G,D,E);

% Sort By Date, Return Indices
[Bcalltimes,sidx] = sort(iso_to_datetime(Bcalls.Var1));
Bcalls_sorted = Bcalls(sidx,:);

[Gcalltimes,sidx] = sort(iso_to_datetime(Gcalls.Var1));     
Gcalls_sorted = Gcalls(sidx,:);

[Dcalltimes,sidx] = sort(iso_to_datetime(Dcalls.Var1));                  
Dcalls_sorted = Dcalls(sidx,:);

[Ecalltimes,sidx] = sort(iso_to_datetime(Ecalls.Var1));                  
Ecalls_sorted = Ecalls(sidx,:);

%times differences between events
Bcalltimes_diff = seconds(diff(Bcalltimes));
Gcalltimes_diff = seconds(diff(Gcalltimes));
Dcalltimes_diff = seconds(diff(Dcalltimes));
Ecalltimes_diff = seconds(diff(Ecalltimes));

%load percentages
window = 10;
%Acallload = calc_load(Acalltimes,window);
Bcallload = calc_load(Bcalltimes,window);
Gcallload = calc_load(Gcalltimes,window);
Dcallload = calc_load(Dcalltimes,window);
Ecallload = calc_load(Ecalltimes,window);



%% Plot the load of the micro services
figure(1)
set(gca,'FontSize',18)
hold on;
plot(Atimes,Aload,'DisplayName','Alpha Load');
plot(Btimes,Bload,'DisplayName','Beta Load');
plot(Gtimes,Gload,'DisplayName','Gamma Load');
plot(Dtimes,Dload,'DisplayName','Delta Load');
plot(Etimes,Eload,'DisplayName','Epsilon Load');
xline(first_error_time,'-.k','Initial Error','LineWidth',3,'DisplayName','First Error');
legend();
title("Load percentage over time");
ylabel(sprintf("Load percent over %d seconds",window));
xlabel("Time");
hold off;


%% Plot the time differences between events per node
figure(2)
set(gca,'FontSize',18)
hold on;
plot(Atimes(2:end),Atimes_diff,'.','DisplayName','Alpha','MarkerSize',20);
plot(Btimes(2:end),Btimes_diff,'.','DisplayName','Beta','MarkerSize',20);
plot(Gtimes(2:end),Gtimes_diff,'.','DisplayName','Gamma','MarkerSize',20);
plot(Dtimes(2:end),Dtimes_diff,'.','DisplayName','Delta','MarkerSize',20);
plot(Etimes(2:end),Etimes_diff,'.','DisplayName','Epsilon','MarkerSize',20);
xline(first_error_time,'-.k','Initial Error','LineWidth',3,'DisplayName','First Error');
legend();
title("Time difference between events over time");
ylabel('Time difference since last event');
xlabel("Time");
hold off;

%% Plot the load of the call times to a node
figure(3)
set(gca,'FontSize',18)
hold on;
%plot(Acalltimes,Acallload,'DisplayName','Alpha Call Load');
plot(Bcalltimes,Bcallload,'DisplayName','Beta Call Load');
plot(Gcalltimes,Gcallload,'DisplayName','Gamma Call Load');
plot(Dcalltimes,Dcallload,'DisplayName','Delta Call Load');
plot(Ecalltimes,Ecallload,'DisplayName','Epsilon Call Load');
xline(first_error_time,'-.k','Initial Error','LineWidth',3,'DisplayName','First Error');
legend();
title("Call Load percentage over time");
ylabel(sprintf("Load percent over %d seconds",window));
xlabel("Time");
hold off;

%% Plot the time differences between calls per node
figure(4)
set(gca,'FontSize',18)
hold on;
plot(Bcalltimes(2:end),Bcalltimes_diff,'.','DisplayName','Beta','MarkerSize',20);
plot(Gcalltimes(2:end),Gcalltimes_diff,'.','DisplayName','Gamma','MarkerSize',20);
plot(Dcalltimes(2:end),Dcalltimes_diff,'.','DisplayName','Delta','MarkerSize',20);
plot(Ecalltimes(2:end),Ecalltimes_diff,'.','DisplayName','Epsilon','MarkerSize',20);
xline(first_error_time,'-.k','Initial Error','LineWidth',3,'DisplayName','First Error');
legend();
title("Time difference between calls over time");
ylabel('Time difference since last event');
xlabel("Time");
hold off;



function out = iso_to_datetime(in)
% converts ISO8601 to matlab date time
    out = datetime(in,'InputFormat','yyyy-MM-dd''T''HH:mm:ss.SSSSSS');

end



function load = calc_load(ts,window)
% Calculates the average load.  
% ts is the timestamps of the events, window is the load window in seconds
% load is the average load in the given window

    load = zeros(length(ts),1);
       
    %get current time
    for i=1:size(ts)
        
        current_time = ts(i);
        window_time = current_time-seconds(window);
        begin_idx = 0; %the index of the begining of the time window
        
        %look back for the begining of the window, store previous index
        for j=i:-1:1
            if ts(j) < window_time
                begin_idx=j-1;
                break
            end
        end
        
        %check to make sure our window is within range
        if begin_idx >= 1
            events_in_window = i-j;
            dt = ts(i)-ts(j);
            load(i) = events_in_window/seconds(dt);
        else
            load(i)=0; 
        end
            
    end
    
    %count the spaces
    

end

function [Acalls,Bcalls,Gcalls,Dcalls,Ecalls] = find_calls(A,B,G,D,E)
%Get time stamps of all calls to a node

    A_idx_beta = find(A.Var5 == "call to beta");
    A_idx_gamma = find(A.Var5 == "call to gamma");
    A_idx_delta = find(A.Var5 == "call to delta");
    A_idx_epsilon = find(A.Var5 == "call to epsilon");

    B_idx_alpha = find(B.Var5 == "call to alpha");
    B_idx_gamma = find(B.Var5 == "call to gamma");
    B_idx_delta = find(B.Var5 == "call to delta");
    B_idx_epsilon = find(B.Var5 == "call to epsilon");

    G_idx_beta = find(G.Var5 == "call to beta");
    G_idx_alpha = find(G.Var5 == "call to alpha");
    G_idx_delta = find(G.Var5 == "call to delta");
    G_idx_epsilon = find(G.Var5 == "call to epsilon");

    D_idx_beta = find(D.Var5 == "call to beta");
    D_idx_gamma = find(D.Var5 == "call to gamma");
    D_idx_alpha = find(D.Var5 == "call to alpha");
    D_idx_epsilon = find(D.Var5 == "call to epsilon");

    E_idx_beta = find(E.Var5 == "call to beta");
    E_idx_gamma = find(E.Var5 == "call to gamma");
    E_idx_delta = find(E.Var5 == "call to delta");
    E_idx_alpha = find(E.Var5 == "call to alpha");

    Acalls = [B(B_idx_alpha,:) ; G(G_idx_alpha,:) ; D(D_idx_alpha,:) ; E(E_idx_alpha,:)];
    Bcalls = [A(A_idx_beta,:) ; G(G_idx_beta,:) ; D(D_idx_beta,:) ; E(E_idx_beta,:)];
    Gcalls = [B(B_idx_gamma,:) ; A(A_idx_gamma,:) ; D(D_idx_gamma,:) ; E(E_idx_gamma,:)];
    Dcalls = [B(B_idx_delta,:) ; G(G_idx_delta,:) ; A(A_idx_delta,:) ; E(E_idx_delta,:)];
    Ecalls = [B(B_idx_epsilon,:) ; G(G_idx_epsilon,:) ; D(D_idx_epsilon,:) ; A(A_idx_epsilon,:)];
    
end